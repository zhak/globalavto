<!DOCTYPE html>
<html>

<head>
    <title>globalavto</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png">
<!--     <link rel="stylesheet" href="/css/slick.css">-->
<!--     <link rel="stylesheet" href="/css/slick-theme.css">-->
<!--    <link rel="stylesheet" href="/css/colorbox.css">-->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">

        <script src='/js/jquery-3.2.1.min.js'></script>
        <script src='/js/tether.min.js'></script>
        <script src='/js/bootstrap.min.js'></script>



<!--    <script src='/js/libs/jquery-ui.min.js'></script>-->
<!--    <script src='/js/libs/jquery.colorbox.js'></script>-->
<!--    <script src='/js/libs/jquery.cookie.js'></script>-->
<!--     <script src='/js/libs/slick.min.js'></script>-->
    <!-- <script src='/js/libs/sweetalert/dist/sweetalert.min.js'></script> -->
    <!-- <script src='/js/libs/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js'></script> -->
    <!-- <script src='/js/libs/wow/dist/wow.min.js'></script> -->
<!--    <script src='/js/script.js'></script>-->

</head>

<body>
    <header>
        <? include "header.php"; ?>
    </header>

    <main>
        <? include "content/" . ($_GET['link'] ? $_GET['link'] : 'main')  .".php"; ?>
    </main>

    <footer>
        <? include "footer.php"; ?>
    </footer>
</body>
<!-- <script type="text/javascript" src="/js/engine1/wowslider.js"></script> -->
<!-- <script type="text/javascript" src="/js/engine1/script.js"></script> -->
</html>
