<div class="header-wrap">
    <div class="container">
        <div class="row">
            <div class="col lang-block">
                <a href="" class="active">Укр</a>
                <a href="">Pус</a>
                <a href="">Eng</a>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-sm-2">
                <div class="logo">
                    <img src="/img/logo.png" alt="">
                </div>
            </div>
            <div class="col-sm-3 logo-text">
                Авто из Европы
            </div>
            <div class="col-sm-3 ">
                <div class="btn-block">
                    <a href="" class="btn btn-red">Консультації та замовлення</a>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="phone-block">
                    <div class="phone-number">
                        <div><i class="fa fa-phone" aria-hidden="true"></i> +38 <span>096</span> 87 85 444</div>
                        <div><i class="fa fa-phone" aria-hidden="true"></i> +38 <span>050</span> 87 85 444</div>
                        <div><i class="fa fa-phone" aria-hidden="true"></i> +38 <span>063</span> 87 85 444</div>
                    </div>
                    <div class="phone-text">
                        <a href="" class="call">Заказать звонок</a>
                        <div class="app">Whatsapp</div>
                        <div class="viber">Viber</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <nav>
                        <ul class="menu-top">
                            <li class="menu-list">
                                <a href="" class="menu-item">Авто из Европы</a>
                                <div class="menu-drop-block">
                                    <ul class="menu-drop">
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                        <li class="menu-drop-list">
                                            <a href="" class="btn btn-drop">Авто із Польщі</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-list">
                                <a href="" class="menu-item">Автодонор</a>
                            </li>
                            <li class="menu-list">
                                <a href="" class="menu-item">Автозапчасти</a>
                            </li>
                            <li class="menu-list">
                                <a href="" class="menu-item">Растаможка</a>
                            </li>
                            <li class="menu-list">
                                <a href="" class="menu-item">Авто для нерезидента</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-1">
                    <div class="add-menu">+</div>
                    <div class="menu-drop-block">
                        <ul class="menu-drop">
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Автоплощадки Європи</a>
                            </li>
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Візи шенген</a>
                            </li>
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Калькулятор Реєстрації</a>
                            </li>
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Алкогольний калькулятор</a>
                            </li>
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Перевірити він код</a>
                            </li>
                            <li class="menu-drop-list">
                                <a href="" class="btn btn-drop">Доставка з Allegro.pl</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
